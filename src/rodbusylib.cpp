/**************************************************************************/
/*   file: rodbusylib.c							  */
/*									  */
/*   Library for the ROD BUSY module ( H/WE by P.Gallno  EP/ATE) 	  */
/*									  */
/*   Author: Markus Joos, CERN EP/ESS					  */
/*									  */
/*****C 2005 - The software with that certain something********************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "rcc_rodbusy/rcc_rodbusy.h"

#define OPEN_CHECK { if (open_count<1) return (RCC_ERROR_RETURN (0, RODBUSY_ISNOTOPEN)); }

// global def of the error function
static err_type rodbusy_err_get(err_pack err, err_str pid, err_str code);
 
static int open_count = 0;
static int mhandle;
static u_long base_add = 0;
static VME_MasterMap_t master_map;
volatile T_rodbusy *rodbusy;


/**************************************/
err_type RODBUSY_Open(u_int vme_address)
/**************************************/
{
  err_type c_stat;

  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Open: function called with vme_address = 0x" << HEX(vme_address));

  //  multiple opens
  if (open_count) 
  {
    open_count++;
    return(RCC_ERROR_RETURN(0, RODBUSY_ISOPEN));
  }

  c_stat = rcc_error_init(P_ID_RODBUSY, rodbusy_err_get);
  if (c_stat != 0) 
  {
    DEBUG_TEXT(DFDB_RCDRODBUSY, 5 ,"RODBUSY_Open: Error from call to rcc_error_init");
    return(RCC_ERROR_RETURN(0, RODBUSY_ERROR_FAIL));
  }
  
  c_stat = VME_Open();
  if (c_stat != VME_SUCCESS)
  {
    DEBUG_TEXT(DFDB_RCDRODBUSY, 5 ,"RODBUSY_Open: Error from call to VME_Open");
    return (RCC_ERROR_RETURN (c_stat, RODBUSY_VMEOPEN));
  }
  
  master_map.vmebus_address   = vme_address;
  master_map.window_size      = 0x1000;
  master_map.address_modifier = VME_A24;
  master_map.options          = 0;
  c_stat = VME_MasterMap(&master_map, &mhandle);
  if (c_stat != VME_SUCCESS)
  {
    DEBUG_TEXT(DFDB_RCDRODBUSY, 5 ,"RODBUSY_Open: Error from call to VME_MasterMap");
    return (RCC_ERROR_RETURN (c_stat, RODBUSY_VMEMSTMAP));
  }

  c_stat = VME_MasterMapVirtualLongAddress(mhandle, &base_add);
  if (c_stat != VME_SUCCESS)
  {
    DEBUG_TEXT(DFDB_RCDRODBUSY, 5 ,"RODBUSY_Open: Error from call to VME_MasterMapVirtualLongAddress");
    return (RCC_ERROR_RETURN (c_stat, RODBUSY_VMEMSTMAP));
  }
    
  rodbusy = (T_rodbusy *)base_add; 
  open_count = 1;
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Open: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/**************************/
err_type RODBUSY_Close(void)
/**************************/
{
  err_type c_stat;

  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Close: function called");
  OPEN_CHECK

  if (open_count > 1)
  {
    open_count-- ;
    return (RCC_ERROR_RETURN (0, RODBUSY_OK));
  }
  
  c_stat = VME_MasterUnmap(mhandle);
  if (c_stat != VME_SUCCESS)
  {
    DEBUG_TEXT(DFDB_RCDRODBUSY, 5 ,"RODBUSY_Close: Error from call to VME_MasterUnmap");
    return (RCC_ERROR_RETURN (c_stat, RODBUSY_VMEMSTUNMAP));
  }

  c_stat = VME_Close();
  if (c_stat != VME_SUCCESS)
  {
    DEBUG_TEXT(DFDB_RCDRODBUSY, 5 ,"RODBUSY_Close: Error from call to VME_Close");
    return (RCC_ERROR_RETURN (c_stat, RODBUSY_VMECLOSE));
  }

  open_count = 0;

  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Close: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/*************************/
err_type RODBUSY_Dump(void)
/*************************/
{
  u_int loop, value;
  u_short data, data2, data3, data4;

  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Dump: function called");
  OPEN_CHECK

  printf("\n-----------------------------------------------------------\n");
  printf("Dumping registers of the RODBUSY module\n");
  
  data = (rodbusy->cr[19]) & 0xff;
  data2 = (rodbusy->cr[21]) & 0xff;
  data3 = (rodbusy->cr[23]) & 0xff;
  value = (data << 16) | (data2 << 8) | data3;
  printf("Manufacturer ID: 0x%08x\n", value);
 
  data = (rodbusy->cr[25]) & 0xff;
  data2 = (rodbusy->cr[27]) & 0xff;
  data3 = (rodbusy->cr[29]) & 0xff;
  data4 = (rodbusy->cr[31]) & 0xff;
  value = (data << 24) | (data2 << 16) | (data3 << 8) | data4;
  printf("Board ID:        0x%08x\n", value);
 
  data = (rodbusy->cr[33]) & 0xff;
  data2 = (rodbusy->cr[35]) & 0xff;
  data3 = (rodbusy->cr[37]) & 0xff;
  data4 = (rodbusy->cr[39]) & 0xff;
  value = (data << 24) | (data2 << 16) | (data3 << 8) | data4;
  printf("Board revision:  0x%08x\n", value);
  
  data = rodbusy->intid;
  data2 = rodbusy->intcsr;
  printf("\nInterrupt vector: 0x%02x Interrupt level: %d\n", data & 0xff, data2 & 0x7);
  printf("SREQ pending: %s  IRQ pending: %s    IRQ enabled: %s\n", (data2 & 0x20)?"Yes":"No ", (data2 & 0x10)?"Yes":"No ", (data2 & 0x8)?"Yes":"No ");
  
  data = rodbusy->sreqcsr;
  printf("SREQ active: %s   SREQ enabled: %s   SW busy: %s  Busy: %s\n", (data & 0x8)?"Yes":"No ", (data & 0x4)?"Yes":"No ", (data & 0x2)?"Yes":"No ", (data & 0x1)?"Yes":"No ");
  
  data = rodbusy->limreg;
  printf("Value of register LIMREG: %d\n", data);
  
  data = rodbusy->ivalreg;
  printf("Value of register IVALREG: %d\n", data);
  
  data = rodbusy->busystate;
  data2 = rodbusy->busymask;
  printf("\nCurrent status of BUSY input lines and mask bits\n");
  printf("Channel: | | | | | | | | | | |1|1|1|1|1|1|\n");
  printf("         |0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|5|\n");
  printf("BUSY     |");
  for(loop = 0; loop < 16; loop++)
    printf("%s|", (data & (0x1<<loop))?"1":"0");
  printf("\n");
  printf("MASK     |");
  for(loop = 0; loop < 16; loop++)
    printf("%s|", (data2 & (0x1<<loop))?"1":"0");
  printf("\n\n");
  
  data = rodbusy->fifwcr & 0x3;
  printf("FIFO / Counter operational mode: ");
  if (data == 0)
    printf("Seqencer & Counters disabled. CNTRST, FIFRST & FIFWEN enabled\n");
  if (data == 1)
    printf("Seqencer disabled. Counters, CNTRST, FIFRST & FIFWEN enabled\n");  
  if (data == 2)
    printf("Seqencer idle. Counters, CNTRST & FIFWEN disabled. FIFRST enabled\n");   
  if (data == 3)
    printf("Seqencer, Counters & FIFRST enabled. CNTRST & FIFWEN disabled\n");
    
  data = rodbusy->fifrcr;
  printf("Value of register FIFRCR: 0x%04x\n", data);
  
  data = rodbusy->seqreg;
  printf("Value of register SEQREG: %d\n", data);

  data = rodbusy->fifff;
  data2 = rodbusy->fifef;
    printf("\nCurrent status of FIFO full / empty bits\n");
  printf("FIFO:    | | | | | | | | | |1|1|1|1|1|1|1|\n");
  printf("         |1|2|3|4|5|6|7|8|9|0|1|2|3|4|5|6|\n");
  printf("FULL     |");
  for(loop = 0; loop < 16; loop++)
    printf("%s|", (data & (0x1<<loop))?"1":"0");
  printf("\n");
  printf("EMPTY    |");
  for(loop = 0; loop < 16; loop++)
    printf("%s|", (data2 & (0x1<<loop))?"1":"0");
  printf("\n\n");
  printf("-----------------------------------------------------------\n\n");

  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Dump: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/**********************************************/
err_type RODBUSY_ReadFIFO(T_rodbusy_fifos *data)
/**********************************************/
{
  u_int value, loop, valid;

  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_ReadFIFO: function called");
  OPEN_CHECK

  for (loop = 0; loop < 16; loop++)
  {
    valid = 0;
    value = rodbusy->fifef & (0x1 << loop);
    while(!value && valid < 511)
    {
      data->fifo[loop].data[valid] = rodbusy->fifo[loop];
      DEBUG_TEXT(DFDB_RCDRODBUSY, 20 ,"RODBUSY_ReadFIFO: 0x" << HEX(data->fifo[loop].data[valid]) << " read from FIFO #" << loop);
      valid++;
      value = rodbusy->fifef & (0x1 << loop);
    }
    data->fifo[loop].nvalid = valid;
  }
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_ReadFIFO: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/*****************************************/
err_type RODBUSY_Init(T_rodbusy_init *data)
/*****************************************/
{  
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Init: function called");
  OPEN_CHECK
  
  if (!data->irq_level)
  { 
    DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Init: The value of data->irq_level is illegal");
    return(RCC_ERROR_RETURN(0, RODBUSY_ILLVAL));
  }
  
  rodbusy->intcsr   = ((data->irq_level & 0x7) | (data->irq_enable & 0x1) << 3);
  rodbusy->intid    = data->irq_vector & 0xff; 
  rodbusy->sreqcsr  = (data->sreq_enable & 0x1) << 2;
  rodbusy->limreg   = data->limit & 0xffff;
  rodbusy->ivalreg  = data->interval & 0xffff;
  rodbusy->busymask = data->busy_mask & 0xffff;  
  rodbusy->fifrcr   = data->fifo_control & 0xffff;  
  rodbusy->fifwcr   = data->mode & 0x3;  
  rodbusy->seqreg   = data->transfer_interval & 0xffff; 

  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Init: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/**************************/
err_type RODBUSY_Reset(void)
/**************************/
{  
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Reset: function called");
  OPEN_CHECK
  rodbusy->swrst = 1;
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_Reset: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/****************************************/
err_type RODBUSY_GenerateSWInterrupt(void)
/****************************************/
{  
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_GenerateSWInterrupt: function called");
  OPEN_CHECK
  rodbusy->swirq = 1;
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_GenerateSWInterrupt: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/***********************************/
err_type RODBUSY_ClearInterrupt(void)
/***********************************/
{    
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_ClearInterrupt: function called");
  OPEN_CHECK
  rodbusy->sreqsetclr = 1;
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_ClearInterrupt: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/************************************************/
err_type  RODBUSY_GetGlobalBusy(u_int *globalbusy)
/************************************************/
{    
  u_short value;
  
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_GetGlobalBusy: function called");
  OPEN_CHECK
  value = rodbusy->sreqcsr;  
  *globalbusy = (value & 0x1);
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_GetGlobalBusy: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/********************************************/
err_type RODBUSY_GetBusy(T_rodbusy_busy *data)
/********************************************/
{    
  u_short value, loop;
  
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_GetBusy: function called");
  OPEN_CHECK
  
  value = rodbusy->busystate;
  for(loop = 0; loop < 16; loop++)
  {
    if(value & (0x1 << loop))
      data->busy[loop] = 1;
    else
      data->busy[loop] = 0;
  } 
  
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_GetBusy: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/************************************************/
err_type RODBUSY_GetBusyMask(T_rodbusy_busy *data)
/************************************************/
{    
  u_short value, loop;
  
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_GetBusyMask: function called");
  OPEN_CHECK
  
  value = rodbusy->busymask;
  for(loop = 0; loop < 16; loop++)
  {
    if(value & (0x1 << loop))
      data->busy[loop] = 1;
    else
      data->busy[loop] = 0;
  } 
  
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_GetBusyMask: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/*********************************/
err_type RODBUSY_CounterReset(void)
/*********************************/
{    
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_CounterReset: function called");
  OPEN_CHECK
  
  if (rodbusy->fifwcr & 0x2)
  {
    DEBUG_TEXT(DFDB_RCDRODBUSY, 5 ,"RODBUSY_CounterReset: Illegal mode");
    return(RCC_ERROR_RETURN(0, RODBUSY_ILLMODE));
  }
    
  rodbusy->cntrst = 1;
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_CounterReset: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/******************************/
err_type RODBUSY_FIFOReset(void)
/******************************/
{    
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_FIFOReset: function called");
  OPEN_CHECK
  rodbusy->fifrst = 1;
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_FIFOReset: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/******************************/
err_type RODBUSY_FIFOWrite(void)
/******************************/
{    
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_FIFOWrite: function called");
  OPEN_CHECK
  
  if (rodbusy->fifwcr & 0x2)
  {
    DEBUG_TEXT(DFDB_RCDRODBUSY, 5 ,"RODBUSY_FIFOWrite: Illegal mode");
    return(RCC_ERROR_RETURN(0, RODBUSY_ILLMODE));
  }
    
  rodbusy->fifwen = 1;
  DEBUG_TEXT(DFDB_RCDRODBUSY, 15 ,"RODBUSY_FIFOWrite: function done");
  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


/***************************************************************/
err_type rodbusy_err_get(err_pack err, err_str pid, err_str code)
/***************************************************************/
{
  strcpy(pid, P_ID_RODBUSY_STR);

  switch(RCC_ERROR_MINOR(err))
  {
    case RODBUSY_OK:          strcpy(code, RODBUSY_OK_STR);          break;
    case RODBUSY_ISOPEN:      strcpy(code, RODBUSY_ISOPEN_STR);      break;
    case RODBUSY_ISNOTOPEN:   strcpy(code, RODBUSY_ISNOTOPEN_STR);   break;
    case RODBUSY_VMEOPEN:     strcpy(code, RODBUSY_VMEOPEN_STR);     break;
    case RODBUSY_VMEMSTMAP:   strcpy(code, RODBUSY_VMEMSTMAP_STR);   break;
    case RODBUSY_VMEMSTUNMAP: strcpy(code, RODBUSY_VMEMSTUNMAP_STR); break;
    case RODBUSY_VMECLOSE:    strcpy(code, RODBUSY_VMECLOSE_STR);    break;
    case RODBUSY_ILLVAL:      strcpy(code, RODBUSY_ILLVAL_STR);      break;
    case RODBUSY_ILLMODE:     strcpy(code, RODBUSY_ILLMODE_STR);     break;
    default:                  strcpy(code, RODBUSY_NO_CODE_STR);
                              return(RCC_ERROR_RETURN(0, RODBUSY_NO_CODE)); break;
  }

  return(RCC_ERROR_RETURN(0, RODBUSY_OK));
}


