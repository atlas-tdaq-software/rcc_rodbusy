/************************************************************************/
/*									*/
/* File: test_rodbusy.c							*/
/*									*/
/* This is the test program for the rcc_rodbusy library			*/
/*									*/
/* Author: Markus Joos, CERN EP/ESS					*/
/*									*/
/**************** C 2002 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "rcc_error/rcc_error.h"
#include "rcc_rodbusy/rcc_rodbusy.h"
#include "ROSGetInput/get_input.h"

// Prototypes
int func_menu(void);
int mainhelp(void);

/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
  return(0);
}


/************/
int main(void)
/************/
{
  int fun = 2;
  
  printf("\n\n\nThis is the test program for the RODBUSY library \n");
  printf("==============================================++=====\n");
  
  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                        2 Test functions\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) func_menu();
  }
  
  return(0);
}  
   
/*****************/
int func_menu(void)
/*****************/
{
  int fun = 1;
  u_int ret = 0, vmead, loop;
  T_rodbusy_fifos rbfifos;
  T_rodbusy_busy rbbusy;
  T_rodbusy_init rbinit;
  static u_int irq_vector = 0x55, irq_level = 3, irq_enable = 1, sreq_enable = 1;
  static u_int limit = 0x100, interval = 0x200, busy_mask = 0xffff;
  static u_int fifo_control = 0, mode = 1, transfer_interval = 0xffff;


  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:\n");
    printf("   1 RODBUSY_Open               2 RODBUSY_Close\n");
    printf("   3 RODBUSY_Init               4 RODBUSY_ReadFIFO\n");
    printf("   5 RODBUSY_Reset              6 RODBUSY_GenerateSWInterrupt\n");
    printf("   7 RODBUSY_ClearInterrupt     8 RODBUSY_GetBusy\n");
    printf("   9 RODBUSY_CounterReset      10 RODBUSY_FIFOReset\n");
    printf("  11 RODBUSY_FIFOWrite         12 RODBUSY_Dump\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun); 
     
    if (fun == 1)
    {  
      printf("Enter the A24 VMEbus base address of the ROD BUSY: ");
      vmead = gethexd(0x600000);
      ret = RODBUSY_Open(vmead);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
    
    if (fun == 2)
    {  
      ret = RODBUSY_Close();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
  
    if (fun == 3)
    {  
      printf("Enter the IRQ vector ");
      irq_vector = gethexd(irq_vector);
  
      printf("Enter the IRQ level ");
      irq_level = getdecd(irq_level);
      
      printf("Enable IRQ (1=yes  0=no): ");
      irq_enable = getdecd(irq_enable);
  
      printf("Enable SREQ (1=yes  0=no): ");
      sreq_enable = getdecd(sreq_enable);
  
      printf("Enter the value for the LIMREG register: ");
      limit = gethexd(limit);
       
      printf("Enter the value for the IVALREG register: ");
      interval = gethexd(interval);
      
      printf("Enter the value for the BUSYMASK register: ");
      busy_mask = gethexd(busy_mask);
      
      printf("Enter the value for the FIFRCR register: ");
      fifo_control = gethexd(fifo_control);
      
      printf("Select the operational mode: \n");
      printf("0 = Seqencer & Counters disabled. CNTRST, FIFRST & FIFWEN enabled\n");
      printf("1 = Seqencer disabled. Counters, CNTRST, FIFRST & FIFWEN enabled\n");
      printf("2 = Seqencer idle. Counters, CNTRST & FIFWEN disabled. FIFRST enabled\n");
      printf("3 = Seqencer, Counters & FIFRST enabled. CNTRST & FIFWEN disabled\n");
      printf("Your choice: ");
      mode = getdecd(mode);
      
      rbinit.irq_vector = irq_vector;
      rbinit.irq_level = irq_level;
      rbinit.irq_enable = irq_enable;
      rbinit.sreq_enable = sreq_enable;
      rbinit.limit = limit;
      rbinit.interval = interval;
      rbinit.busy_mask = busy_mask;  
      rbinit.fifo_control = fifo_control;  
      rbinit.mode = mode;  
      rbinit.transfer_interval = transfer_interval;  
  
      ret = RODBUSY_Init(&rbinit);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
    
    if (fun == 4)
    {  
      ret = RODBUSY_ReadFIFO(&rbfifos);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      for(loop = 0; loop < 16; loop++)
        printf("FIFO %d returned %d words\n", loop, rbfifos.fifo[loop].nvalid);
    } 

    if (fun == 5)
    {
      ret = RODBUSY_Reset();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    } 

    if (fun == 6)
    {
      ret = RODBUSY_GenerateSWInterrupt();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    } 

    if (fun == 7)
    {
      ret = RODBUSY_ClearInterrupt();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    } 

    if (fun == 8)
    {
      ret = RODBUSY_GetBusy(&rbbusy);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      for(loop = 0; loop < 16; loop++)
        printf("Channel %d is %s\n", loop, rbbusy.busy[loop]?"busy":"not busy");
    } 

    if (fun == 9)
    {
      ret = RODBUSY_CounterReset();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    } 

    if (fun == 10)
    {
      ret = RODBUSY_FIFOReset();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    } 

    if (fun == 11)
    {
      ret = RODBUSY_FIFOWrite();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
    
    if (fun == 12)
    {  
      ret = RODBUSY_Dump();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
  }
  return(0);
}
