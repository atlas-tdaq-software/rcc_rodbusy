/************************************************************************/
/*									*/
/* File: tb_test_rodbusy.c	      					*/
/*									*/
/* This is the program to select BUSY signals at the TB			*/
/*									*/
/* Author: Johannes Haller, CERN PH-ATR                                 */
/* adatpted from test_rodbusy.c by Markus Joos, CERN EP/ESS		*/
/*									*/
/************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "rcc_error/rcc_error.h"
#include "rcc_rodbusy/rcc_rodbusy.h"
#include "ROSGetInput/get_input.h"

/************/
int main(void)
/************/
{
  u_int ret = 0, bre = 0, input, vmead, loop;
  int fun = 3, channel = 0, enable = 0 ;
  T_rodbusy_busy rbbusy;
  T_rodbusy_busy rbbusymask;
  T_rodbusy_init rbinit;
  static u_int busy_mask = 0x0000;
  printf("\n\n\n============================================================\n");
  printf("\nProgram to select BUSY signals at the ATLAS combined TB \n");
  printf("\n=================================================================\n");  
  // get VME address
  // printf("=======================================================\n\n");
  // printf("Enter the A24 VMEbus base address of the ROD BUSY: ");
  //vmead = gethexd(0xff0000);
  vmead = 0xff0000;
  ret = RODBUSY_Open(vmead);
  if (ret)
    {
      rcc_error_print(stdout, ret);
      
      return(-1);
    }
  
  // reset
  //  ret = RODBUSY_Reset();
  //if (ret)
  //  {
  //    rcc_error_print(stdout, ret);
  //    return(-1);
  //  }
  

  while (fun != 0)  
    {
      printf("\n");
      printf("Select:\n");
      printf("   1 Reset               \n");
      printf("   2 Enable/Disable \n");
      printf("   3 Status    \n");
      printf("   4 Help\n");
      printf("   0 Exit\n");
      printf("Your choice ");
      fun = getdecd(fun); 
      
      if (fun == 1) {
	ret = RODBUSY_Reset();
	if (ret) {
	  rcc_error_print(stdout, ret);
	  return(-1);
	}
      } 
      
      if (fun == 2) {  
	channel=0;
	enable=0;
	bre=0;
	while (bre != 1){
	  printf("Which channel: (0-15) ");
	  input = getdecd(channel);
	  if (input>=0 && input<=15) {
	    bre=1;
	  } else {
	    printf("Wrong channel number !!\n");
	  }
	}
	channel=input;
	
	bre=0;
	while (bre != 1) {
	  printf("0 disable; 1 enable: ");
	  input = getdecd(enable);
	  if (input==0 || input==1) {
	    bre=1;
	  } else {
	    printf("choose either 0 or 1 !!\n");
	  }
	}
	enable = input;
	
	rbbusymask.busy[channel] = enable ;
	
	busy_mask = 0x0000;
	for(loop = 0; loop < 16; loop++) {
	  if (rbbusymask.busy[loop] == 1) {
	    busy_mask = busy_mask + (0x1<<loop)  ;
	  } else {
	    busy_mask = busy_mask + (0x0<<loop)  ;
	  }
	}
	
	rbinit.busy_mask = busy_mask;  
	
	ret = RODBUSY_Init(&rbinit);
	if (ret) {
	  rcc_error_print(stdout, ret);
	  return(-1);
	}
      }
      
      if (fun == 3) {
	
	ret = RODBUSY_GetBusy(&rbbusy);
	if (ret) {
	  rcc_error_print(stdout, ret);
	  return(-1);
	}
	
	ret = RODBUSY_GetBusyMask(&rbbusymask);
	if (ret) {
	  rcc_error_print(stdout, ret);
	  return(-1);
	}
  printf("\n====================================================================\n");
	printf("  Channel <--> Detector \n");
	printf("------------------------------\n");
	printf("   0  MUCTPI   1 TGC-I     2 TGC-J\n"); 
	printf("   3  MDT      4 Tile/LAr  5 Spill\n");
	printf("   6  RPC      7 TRT       8 Pixel\n");
	printf("   9  TGC lg  10 L1Calo   11 CSC    \n");

	printf("\nCurrent status of BUSY input lines and mask bits:\n");
	printf("------------------------------------------\n");
	printf("Channel: | | | | | | | | | | |1|1|1|1|1|1|\n");
	printf("         |0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|5|\n");
	printf("------------------------------------------\n");
	printf("BUSY:    |");
	for(loop = 0; loop < 16; loop++)
	  printf("%d|", rbbusy.busy[loop]);
	printf("   0/1  not busy/ busy \n");
	printf("MASK:    |");
	for(loop = 0; loop < 16; loop++)
	  printf("%d|", rbbusymask.busy[loop]);  
	printf("   0/1  disabled/ enabled\n");
  printf("====================================================================\n");
      } 
      if (fun == 4) {  
	printf("\n=========================================================================\n");
	printf("Contact Johannes.Haller@cern.ch for help\n");
	printf("   or call 71262 or 076-533 11 12\n");
	printf("=========================================================================\n\n");
      }
    }
  return(0);
}
