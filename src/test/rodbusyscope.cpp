/****************************************************************/
/* file: rodbusyscope.c                                         */
/*                                                              */
/* Author: Markus Joos, EP/ESS                                  */
/*                                                              */
/* 24. Jun. 02 MAJO created                                     */
/*                                                              */
/*******C 2000 - A nickel program worth a dime*******************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include "rcc_error/rcc_error.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "vme_rcc/vme_rcc.h"

// Prototypes
int mainhelp(void);
int dumpreg(void);
int intgen(void);
void SigQuitHandler(int signum);
void rodbusy_signal_handler(int signum);
int setbusymask(void);
int intgenlemo(void);
int clrpend(void);
void rodbusy_signal_handler(int signum);
int setdebug(void);


// Constants
#define SIGNUM  42

// Globals
static int ihandle, handle, level = 3, icont = 0, cont = 0;
static struct sigaction sa, sa2;
u_int dblevel = 0, dbpackage = DFDB_RCDRODBUSY;

/************/
int main(void)
/************/
{
  int fun = 1;
  u_int ret;
  int stat;
  static VME_MasterMap_t master_map = {0x00600000, 0x1000, VME_A24, 0};
  
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler =  rodbusy_signal_handler;
  // Dont block any signal in intercept handler
  if (sigaction(SIGNUM, &sa, NULL) < 0) 
  {
    perror("sigaction GL_Signal ");
    exit(1);
  }

  // Signal handler for ctrl + //
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = SigQuitHandler;
  stat = sigaction(SIGQUIT, &sa2, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", stat);
    exit(1);
  }

  ret = VME_Open();
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
   
  printf("Enter the A24 VMEbus address of the ROD-BUSY: ");
  master_map.vmebus_address = gethexd(master_map.vmebus_address);
  ret = VME_MasterMap(&master_map, &handle);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
      
  //Disable the Service Request
  ret = VME_WriteSafeUShort(handle, 0x90, 0);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }


  fun = 1;
  printf("\n\n\nThis is RODBUSYSCOPE for VMEbus and a RCC\n");
  printf("=========================================\n");

  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                       2 Dump registers\n");
    printf("   3 Generate interrupts (S/W)  4 Generate interrupts (Lemo)\n");  
    printf("   5 Clear pending interrupt    6 Set busy mask\n");
    printf("   7 Set debug parameters\n");
    printf("   0 Quit\n");
    printf("Your choice: ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) dumpreg();
    if (fun == 3) intgen();
    if (fun == 4) intgenlemo();
    if (fun == 5) clrpend();
    if (fun == 6) setbusymask();
    if (fun == 7) setdebug();
  }  
  
  ret = VME_MasterUnmap(handle);
  if (ret)
    VME_ErrorPrint(ret);
  
  ret = VME_Close();
  if (ret)
    VME_ErrorPrint(ret);
  
  exit(0);
}  


/****************/
int setdebug(void)
/****************/
{
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/***************/
int dumpreg(void)
/***************/
{
  u_short data;
  u_int ret;

  printf("=============================================================\n");
  ret = VME_ReadSafeUShort(handle, 0x86, &data);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
    
  printf("\nRegister INTID     = 0x%08x\n", data);  
  printf("Interrupt vector:  0x%02x\n", data & 0xff);

  ret = VME_ReadSafeUShort(handle, 0x84, &data);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  printf("\nRegister INTCSR    = 0x%08x\n", data);
  printf("Interrupt enabled: %s\n", (data & 0x8)?"yes":"no");
  printf("Interrupt level:   %d\n", data & 0x7);
 
  ret = VME_ReadSafeUShort(handle, 0x90, &data);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  printf("\nRegister SREQCSR   = 0x%08x\n", data);
  printf("Busy:              %s\n", (data &0x1)?"active":"not active");
  printf("SW Busy:           %s\n", (data &0x2)?"set":"not set");
  printf("SREQ enabled:      %s\n", (data &0x4)?"yes":"no");
  printf("SREQ active:       %s\n", (data &0x8)?"yes":"no");
  
  ret = VME_ReadSafeUShort(handle, 0x94, &data);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  printf("\nRegister LIMREG    = 0x%08x\n", data);
  
  ret = VME_ReadSafeUShort(handle, 0x96, &data);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  printf("\nRegister IVALREG   = 0x%08x\n", data);
   
  ret = VME_ReadSafeUShort(handle, 0x98, &data);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  printf("\nRegister BUSYSTATE = 0x%08x\n", data);
  
  ret = VME_ReadSafeUShort(handle, 0x9a, &data);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  printf("\nRegister BUSYMASK  = 0x%08x\n", data);
  printf("=============================================================\n");
  return(0);
}


/*******************/
int setbusymask(void)
/*******************/
{
  static u_int bmask = 0;
  u_int ret;
  u_short data;
  
  printf("=============================================================\n");
  printf("Enter the 16-bit busy mask: ");
  bmask = gethexd(bmask);

  ret = VME_WriteSafeUShort(handle, 0x9a, bmask);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }

  ret = VME_ReadSafeUShort(handle, 0x98, &data);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  printf("Register BUSYSTATE = 0x%08x\n", data);
  printf("=============================================================\n");
  return(0); 
}


/**************/
int intgen(void)
/**************/
{
  static u_short nint = 1, vector = 0x55;
  VME_InterruptList_t irq_list;
  u_int ret;
 
  //Reset the card
  ret = VME_WriteSafeUShort(handle, 0x80, 0);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
    
  printf("Enter the interrupt level: ");
  level = getdecd(level);

  printf("Enter the interrupt vector: ");
  vector = gethexd(vector);
  
  printf("How many interrupts (0=until ctrl+\\): ");
  nint = getdecd(nint);

  ret = VME_WriteSafeUShort(handle, 0x86, vector);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }  
  
  ret = VME_WriteSafeUShort(handle, 0x84, (level + 0x8));  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = vector;
  irq_list.list_of_items[0].level = level;
  irq_list.list_of_items[0].type = VME_INT_RORA;
  ret = VME_InterruptLink(&irq_list, &ihandle);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }

  ret = VME_InterruptRegisterSignal(ihandle, SIGNUM);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  cont = 1;
  while(cont)
  {
    icont = 1;
    
    // Generate interrupt
    ret = VME_WriteSafeUShort(handle, 0x82, 0);  
    if (ret)
    {
      VME_ErrorPrint(ret);
      return(-1);
    }  
    
    // Wait for the interrupt
    while(icont)
    {
      //printf("Waiting..\n");
    }

    // Clear the interrupt
    ret = VME_WriteSafeUShort(handle, 0x84, (level + 0x8));  
    if (ret)
    {
      VME_ErrorPrint(ret);
      return(-1);
    }
    
    // Re-enable the level
    ret = VME_InterruptReenable(ihandle);
    if (ret)
    {
      VME_ErrorPrint(ret);
      return(-1);
    }
    
    if (nint == 1)
      break;
    if (nint)
      nint--;
  }
  
  ret = VME_InterruptUnlink(ihandle);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
    
  return(0);
}


/******************/
int intgenlemo(void)
/******************/
{
  static u_short nint = 1, vector = 0x55;
  VME_InterruptList_t irq_list;
  u_int ret;

  //Reset the card
  ret = VME_WriteSafeUShort(handle, 0x80, 0);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
 
  printf("Enter the interrupt level: ");
  level = getdecd(level);

  printf("Enter the interrupt vector: ");
  vector = gethexd(vector);
  
  printf("How many interrupts (0=until ctrl+\\): ");
  nint = getdecd(nint);

  ret = VME_WriteSafeUShort(handle, 0x86, vector);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }  
  
  ret = VME_WriteSafeUShort(handle, 0x84, (level + 0x8));  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = vector;
  irq_list.list_of_items[0].level = level;
  irq_list.list_of_items[0].type = VME_INT_RORA;
  ret = VME_InterruptLink(&irq_list, &ihandle);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }

  ret = VME_InterruptRegisterSignal(ihandle, SIGNUM);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
    
  //Program the BUSYMASK
  ret = VME_WriteSafeUShort(handle, 0x9a, 0x4000);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  //Program the Counters
  ret = VME_WriteSafeUShort(handle, 0x94, 0x100);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  ret = VME_WriteSafeUShort(handle, 0x96, 0x200);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  //Enable the Service Request
  ret = VME_WriteSafeUShort(handle, 0x90, 0x4);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  cont = 1;
  icont = 1;
  while(cont)
  {
    // Wait for the interrupt
    while(icont);

    icont = 1;
    // Clear the interrupt
    ret = VME_WriteSafeUShort(handle, 0x84, (level + 0x8));  
    if (ret)
    {
      VME_ErrorPrint(ret);
      return(-1);
    }
 
    // Re-enable the level
    ret = VME_InterruptReenable(ihandle);
    if (ret)
    {
      VME_ErrorPrint(ret);
      return(-1);
    }
    
    if (nint == 1)
      break;
    if (nint)
      nint--;
  }

  ret = VME_WriteSafeUShort(handle, 0x9a, 0x0);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  //Disable the Service Request
  ret = VME_WriteSafeUShort(handle, 0x90, 0x0);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  ret = VME_WriteSafeUShort(handle, 0x84, (level + 0x8));  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  
  // Re-enable the level
  ret = VME_InterruptReenable(ihandle);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }

  ret = VME_InterruptUnlink(ihandle);
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
    
  return(0);
}


/***************/
int clrpend(void)
/***************/
{
  u_int ret;

  // Clear the interrupt
  ret = VME_WriteSafeUShort(handle, 0x84, 0);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    return(-1);
  }
  return(0);
}


/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
  cont = 0;
  alarm(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Call Markus Joos, 72364, 160663 if yon need help.\n");
  printf("=========================================================================\n\n");
  return(0);
}


/*************************************/
void rodbusy_signal_handler(int signum)
/*************************************/
{
  if (signum != SIGNUM )  
    printf("unexpected signal\n") ;
  else
  {
    //printf("interrupt received\n");
    icont = 0;
  }
}

