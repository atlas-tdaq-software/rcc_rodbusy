/****************************************************************/
/* file: rod_busy_test.cpp                                      */
/*                                                              */
/* Program to test ATLAS ROD BUSY module in stand alone mode 	*/
/*								*/
/* Author: Per Gallno						*/
/* Ported from CATY to C by: Markus Joos, PH/ESS                */
/*                                                              */
/*******C 2007 - A nickel program worth a dime*******************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdint.h>
#include <sys/types.h>
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "vme_rcc/vme_rcc.h"
#include "rcc_rodbusy/rcc_rodbusy.h"


//Globals
u_int ret, cont;
int handle = 999;
static VME_MasterMap_t master_map = {0x600000, 0x1000, VME_A24, 0};
T_rodbusy *regs;
static struct sigaction sa, sa2;


//Macros
#define ISMAPPED {if (handle == 999) {printf("You have not yet executed test 4\n"); return;}}
#define ISOK     {if (ret) {VME_ErrorPrint(ret); return;}}
#define PTO      {char cc[9]; printf("Press<return> to continue\n"); fgets(cc, 9, stdin);}
#define PTO2     {char cc[9]; fgets(cc, 9, stdin);}


//Constants
#define CERNID 0x00080030
#define SIGNUM 42


//Prototypes
void test_menu(void);
void test_sequential(void);
void SigQuitHandler(int signum);
void vme_signal_handler(int signum);
void rdfifos(void);
void errfullempty (u_int emptyfl, u_int fullfl);
void readprom4(u_int offset, u_int *concat);
void writeprom4(u_int offset, u_int concat);
void read_prom(void);
void write_prom(void);
void binstring(u_char *bits, u_int data, u_int nbits);
void testreg(u_int offset);
void test_1(void);
void test_2(void);
void test_3(void);
void test_4(void);
void test_5(void);
void test_6(void);
void test_7(void);
void test_8(void);
void test_9(void);
void test_10(void);
void test_11(void);
void test_12(void);
void test_13(void);
void test_14(void);
void test_15(void);
void test_16(void);
void test_17(void);
void test_18(void);
void test_19(void);
void test_20(void);
void test_21(void);
void test_22(void);
void test_23(void);
void test_24(void);
void test_25(void);
void test_26(void);
void test_27(void);

/************/
int main(void)
/************/
{
  int stat, int_handle;
  char mode[9];
  VME_InterruptList_t irq_list;

  ret = ts_open(1, TS_DUMMY);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  regs = (T_rodbusy *)0;
  
  ret = VME_Open();
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  
  // Signal handler for ctrl + //
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = SigQuitHandler;
  stat = sigaction(SIGQUIT, &sa2, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler for ctrl + // (error=%d)\n", stat);
    exit(1);
  }  

  // Signal handler for VMEbus interrupts
  sigemptyset(&sa.sa_mask) ;
  sa.sa_flags = 0;
  sa.sa_handler =  vme_signal_handler;
  stat = sigaction(SIGNUM, &sa, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler for VMEbus interrupts (error=%d)\n", stat);
    exit(1);
  }  

  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = 0xab;
  irq_list.list_of_items[0].level = 3;
  irq_list.list_of_items[0].type = VME_INT_ROAK;
  ret = VME_InterruptLink(&irq_list, &int_handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }  

  ret = VME_InterruptRegisterSignal(int_handle, SIGNUM);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }  
  
  //MJ: At this point Per's programm disables intherrupts
  //vw16 INTCSR,0 ;disable interrupts
  //MJ: We cannot do this as we don't know the base address yet.
  
  printf("START OF TEST PROGRAM\n");
  printf("THE PROGRAM IS EITHER MENU DRIVEN [m] OR SEQUENTIAL [s]\n");
  printf("SELECT [m] or [s] : ");
  fgets(mode, 9, stdin);

  if (mode[0] == 'm' || mode[0] == 'M')
    test_menu();
  else if (mode[0] == 's' || mode[0] == 'S')
    test_sequential(); 
  else
    printf("Have a nice day\n");
  
  ret = ts_close(TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);
  
  if (handle != 999)
  {  
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
  }
  
  ret = VME_Close();
  if (ret)
    VME_ErrorPrint(ret);
}



/******************/
void test_menu(void)
/******************/
{
  static int fun = 1;

  while(fun != 0)
  {  
    printf("\n\nSELECT ONE OF THE FOLLOWING TASKS:\n");
    
    printf("CHECK SUPPLY VOLTAGES..........[ 1]   CHECK REFERENCE VOLTAGE........[ 2]\n");   
    printf("CHECK CLOCK DISTRIBUTION.......[ 3]   SELECT MODULE BASE ADDRESS.....[ 4]\n");   
    printf("SELECT MODULE AM CODE..........[ 5]   CHECK MODULE VME RESPONSE......[ 6]\n");   
    printf("WR/RD ID-PROM..................[ 7]   WR/RD BUSY MASK................[ 8]\n");   
    printf("READ INPUT LINES...............[ 9]   WR/RD SREQ CSR.................[10]\n"); 
    printf("NIM/TTL LEVEL CHECK............[11]   WR/RD SREQ LIMIT REG...........[12]\n");   
    printf("WR/RD SREQ INTERVAL REG........[13]   WR/RD STATUS/ID REG............[14]\n");
    printf("WR/RD INTERRUPTER CSR..........[15]   GENERATE SW INTERRUPT..........[16]\n");
    printf("RESET ALL COUNTERS.............[17]   RESET ALL FIFO POINTERS........[18]\n");
    printf("TRANSFER COUNT TO FIFO's.......[19]   WR/RD COUNTER/FIFO WR CR. .....[20]\n");
    printf("WR/RD FIFO RD CR...............[21]   WR/RD SEQ TRF INTERVAL REG.....[22]\n");
    printf("READ FIFO FULL/EMPTY FLAGS.....[23]   CHECK FIFO FULL/EMPTY FLAGS....[24]\n");
    printf("CHECK COUNTER/FIFO DATA LINES..[25]   GENERATE MODULE RESET..........[26]\n");
    printf("COMPLETE TEST OF SREQ + IRQ... [27]\n");
    
    printf("QUIT PROGRAM...................[ 0]\n");
    printf("Your choice: ");
    fun = getdecd(fun);
    printf("\n\n");
    if (fun == 1) test_1();
    if (fun == 2) test_2();
    if (fun == 3) test_3();
    if (fun == 4) test_4();
    if (fun == 5) test_5();
    if (fun == 6) test_6();
    if (fun == 7) test_7();
    if (fun == 8) test_8();
    if (fun == 9) test_9();
    if (fun == 10) test_10();
    if (fun == 11) test_11();
    if (fun == 12) test_12();
    if (fun == 13) test_13();
    if (fun == 14) test_14();
    if (fun == 15) test_15();
    if (fun == 16) test_16();
    if (fun == 17) test_17();
    if (fun == 18) test_18();
    if (fun == 19) test_19();
    if (fun == 20) test_20();
    if (fun == 21) test_21();
    if (fun == 22) test_22();
    if (fun == 23) test_23();
    if (fun == 24) test_24();
    if (fun == 25) test_25();
    if (fun == 26) test_26();
    if (fun == 27) test_27();
  }
}


/************************/
void test_sequential(void)
/************************/
{
  test_1();
  test_2();
  test_3();
  test_4();
  test_5();
  test_6();
  test_7();
  test_8();
  test_9();
  test_10();
  test_11();
  test_12();
  test_13();
  test_14();
  test_15();
  test_16();
  test_17();
  test_18();
  test_19();
  test_20();
  test_21();
  test_22();
  test_23();
  test_24();
  test_25();
  test_26();
  test_27();
}


/***************/
void test_1(void)
/***************/
{
  printf("                SUPPLY VOLTAGE CHECK\n\n");
  printf("1. Put the module on an extender and turn ON the crate power\n");
  printf("2. Measure between GND and some IC's VCC pins: +5.0V +/- 0.1\n");
  printf("3. Measure between GND and IC11 (LM741) pin 7: +12.0V +/- 0.1\n");
  printf("4. Measure between GND and IC11 (LM741) pin 4: -12.0V +/- 0.1\n");
}


/***************/
void test_2(void)
/***************/
{
  printf("           INPUT REFERENCE VOLTAGE CHECK\n\n");
  printf("      !!!!! ROUTINE NOT YET IMPLEMENTED !!!!!\n");
}


/***************/
void test_3(void)
/***************/
{
  printf("                CLOCK DISTRIBUTION CHECK\n\n");
  printf("1. Put the module on an extender and turn ON the crate power\n");
  printf("2. With an oscilloscope + probe (10 Meg / 10X) check clock at:\n");
  printf("   - R56, R57, R59, R60, R61, R64 and R88\n");
  printf("   The clock should be a smooth 10 MHz square wave with a 4 Volt swing\n");
}


/***************/
void test_4(void)
/***************/
{

  printf("             SET/ADJUST THE MODULE BASE ADDRESS\n");
  printf("Enter the A24 VMEbus address of the ROD-BUSY: ");
  master_map.vmebus_address = gethexd(master_map.vmebus_address);
  if (master_map.vmebus_address > 0xe00000)
  {
    printf("ERROR!!! THE VALUE MUST BE > '0xE00000\n");
    return;
  }
  
  ret = VME_MasterMap(&master_map, &handle);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  printf("The module base address is set to 0x%06x\n", master_map.vmebus_address);
  printf("!!!! CHECK SWITCH SETTINGS [SW1..SW4] ON MODULE !!!\n");
}


/***************/
void test_5(void)
/***************/
{
  printf("There is nothing to be done here");
  printf("AM code 0x39 will be used exclusively\n");
}


/***************/
void test_6(void)
/***************/
{
  ISMAPPED
  printf("               CHECK VME ACCESS\n\n");
  printf("   Press <ctrl>+\\ to stop the routine.\n");
  
  cont = 1;
  while(cont)
  {
    ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->swrst, 1);
    if (ret)
      printf("!!! Recheck ADDRESS and AM settings !!!\n");
  }
  printf("Access OK\n");
}


/***************/
void test_7(void)
/***************/
{
  char mode[9];
  
  ISMAPPED
  printf("      READ/WRITE IDPROM\n");
  read_prom();
  
  printf("To write IDPROM enter [w], to quit [q]\n");
  fgets(mode, 9, stdin);

  if (mode[0] == 'w' || mode[0] == 'W')
  {
    write_prom();
    read_prom();
  } 
}


/***************/
void test_8(void)
/***************/
{
  ISMAPPED 
  printf(" ROUTINE TO TEST INPUT MASK REGISTER\n");
  testreg((uintptr_t)&regs->busymask);
}


/***************/
void test_9(void)
/***************/
{
  ISMAPPED
  printf(" ROUTINE TO READ BUSY INPUT REGISTER AND WRITE INPUT TEST REGISTER\n");
  printf("REMOVE EXTERNAL BUSY LINES\n");
  testreg((uintptr_t)&regs->busystate);
}

/****************/
void test_10(void)
/****************/
{
  u_int errflag = 0;
  u_short rpatt, rpatt1, rpatt2;
  
  ISMAPPED
  printf(" ROUTINE TO TEST SERVICE REQUESTER CONTROL REGISTER\n");
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->limreg, 1);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->limreg, 1);
  ISOK  
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 0);
  ISOK   
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->sreqcsr, &rpatt1);
  ISOK  

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 6);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->sreqcsr, &rpatt2);
  ISOK 

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 0);
  ISOK

  if (rpatt1 & 0x6)
    errflag = 1;

  if ((rpatt2 & 0x6) != 0x6)    
    errflag = 1;
 
  if (errflag)
    printf("!!! HARDWARE ERROR !!!\n");
  else
    printf("SREQ CONTROL BITS IN REGISTER ARE OK\n");  

  PTO

  printf("TESTING SOFTWARE BUSY CONTROL AND STATUS\n");
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busymask, 0);
  ISOK
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 2);
  ISOK  

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->sreqcsr, &rpatt);
  ISOK  

  printf("   The SW BUSY bit is now SET !\n");
  if (rpatt & 0x1)
    printf("            SW BUSY status is now active\n");
  else
    printf("  !! ERROR !! - BUSY status bit is NOT active\n");

  PTO

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 0);
  ISOK  

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->sreqcsr, &rpatt);
  ISOK 

  printf("   The SW BUSY bit is now CLEARED !\n");

  if (rpatt & 0x1)
    printf("   !! ERROR !! - BUSY status bit is still SET\n");
  else
    printf("            SW BUSY status is now CLEARED\n");

  PTO

  printf("TESTING SREQ SET/CLEAR AND STATUS:\n");

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 4);
  ISOK  
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqsetclr, 2);
  ISOK
   
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->sreqcsr, &rpatt);
  ISOK 

  printf("   The SREQ control bit is now SET !\n");
  if (rpatt & 0x8)
    printf("            SREQ status is now SET\n");
  else
    printf("  !! ERROR !! - SREQ status bit is NOT active\n");

  PTO

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqsetclr, 1);
  ISOK
   
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->sreqcsr, &rpatt);
  ISOK 

  printf("   The SREQ control bit is now CLEARED !\n");
  if (rpatt & 0x8)
    printf("     !! ERROR !! - SREQ status bit is still SET\n");
  else
    printf("            SREQ status is now CLEARED\n");
    
  PTO

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 0);
  ISOK
}


/****************/
void test_11(void)
/****************/
{
  printf("                THIS TEST SHOULD BE DONE AFTER TEST 13 !!!\n\n");
  printf("1. MEASURE BUSY OUT NIM LEVELS ACROSS 50 OHM BY STEPPING\n");
  printf("   THROUGH ROUTINE 13: BUSY = - 0.9 V / NOT BUSY = 0 V\n");
  printf("          !!!   CHECK BOTH OUTPUTS !!! \n\n");
  printf("2. MEASURE BUSY OUT TTL LEVELS WITH OUTPUT CONNECTED TO\n");
  printf("   INPUT 0 (NO 50 OHM TERMINATIONAND) AND BY STEPPING\n");
  printf("   THROUGH ROUTINE 13: BUSY = +0.2 V / NOT BUSY = +0.9 V\n");
  printf("          !!!   CHECK BOTH OUTPUTS !!! \n");
}


/****************/
void test_12(void)
/****************/
{
  printf(" ROUTINE TO TEST SREQ LIMIT REGISTER\n");
  testreg((uintptr_t)&regs->limreg);
}


/****************/
void test_13(void)
/****************/
{
  printf(" ROUTINE TO TEST SREQ INTERVAL REGISTER\n");
  testreg((uintptr_t)&regs->ivalreg);
}


/****************/
void test_14(void)
/****************/
{
  u_int wpatt, errflag = 0;
  u_short rpatt1, rpatt2, rpatt3, rpatt4;   
  
  ISMAPPED
  printf(" ROUTINE TO TEST AND INITIATE STATUS/ID REGISTER\n");
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intid, 0);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intid, &rpatt1);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intid, 0xff);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intid, &rpatt2);
  ISOK
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intid, 0x55);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intid, &rpatt3);
  ISOK
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intid, 0xaa);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intid, &rpatt4);
  ISOK

  if (rpatt1 & 0xff)
  {
    errflag = 1;
    printf("Error: Pattern 1 is 0x%02x instead of 0x00\n", rpatt1 & 0xff); 
  }

  if ((rpatt2 & 0xff) != 0xff)
  {
    errflag = 1;
    printf("Error: Pattern 1 is 0x%02x instead of 0xFF\n", rpatt2 & 0xff); 
  }
  
  if ((rpatt3 & 0xff) != 0x55)
  {
    errflag = 1;
    printf("Error: Pattern 1 is 0x%02x instead of 0x55\n", rpatt3 & 0xff); 
  }
  
  if ((rpatt4 & 0xff) != 0xaa)
  {
    errflag = 1;
    printf("Error: Pattern 1 is 0x%02x instead of 0xAA\n", rpatt4 & 0xff); 
  }

  if (errflag)
    printf("!!! HARDWARE ERROR !!!\n");
  else
    printf(" READ/WRITE STATUS/ID REGISTER IS OK\n");

  PTO

  printf("Enter a byte value in the STATUS/ID Regiter ");
  wpatt = gethexd(0);
  wpatt &= 0xff;

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intid, wpatt);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intid, &rpatt1);
  ISOK

  printf("The value in the STATUS/ID Regiter is now set to: 0x%02x\n", rpatt1 & 0xff);
}


/****************/
void test_15(void)
/****************/
{
  u_int irqen = 1, errflag = 0;
  u_short rpatt1, rpatt2, rpatt3, rpatt4, wpatt;   

  ISMAPPED
  printf(" ROUTINE TO TEST AND INITIATE INTERRUPTER CSR\n");
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, 0);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intcsr, &rpatt1);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, 7);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intcsr, &rpatt2);
  ISOK
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, 5);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intcsr, &rpatt3);
  ISOK
    
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, 2);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intcsr, &rpatt4);
  ISOK

  if (rpatt1 & 0xf)
  {
    errflag = 1;
    printf("Error: pattern 1 is 0x%01x istead of 0x0\n", rpatt1 & 0xf);
  } 
  
  if ((rpatt2 & 0xf) != 0x7)
  {
    errflag = 1;
    printf("Error: pattern 1 is 0x%01x istead of 0x7\n", rpatt2 & 0xf);
  }
  
  if ((rpatt3 & 0xf) != 0x5)
  {
    errflag = 1;
    printf("Error: pattern 1 is 0x%01x istead of 0x5\n", rpatt3 & 0xf);
  }
  
  if ((rpatt4 & 0xf) != 0x2)
  {
    errflag = 1;
    printf("Error: pattern 1 is 0x%01x istead of 0x2\n", rpatt4 & 0xf);
  }

  if (errflag)
    printf("!!! HARDWARE ERROR !!!\n");
  else  
    printf(" READ/WRITE INTERRUPTER CSR IS OK\n");

  PTO
  
  while(1)
  {
    printf("Enter VME Interrupt level [1..7]: ");
    wpatt = getdecd(1);

    if (wpatt < 1 || wpatt > 7)
    {
      printf("Error: you have to enter a value between 1 and 7\n");
    }
    else
      break;
  }
     
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, wpatt);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intcsr, &rpatt1);
  ISOK

  printf("The VME IRQ Level is now set to: %d", rpatt1 & 0x7);

  printf("Enter '1' to enable VME interrupter, else '0': ");
  irqen = getdecd(irqen);
  irqen = irqen >> 3;
  wpatt = wpatt | irqen; 

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, wpatt);
  ISOK

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intcsr, &rpatt1);
  ISOK

  if (rpatt1 & 8)
    printf("The VME Interrupter is now enabled\n");
  else
    printf("The VME Interrupter is now disabled\n");
}


/****************/
void test_16(void)
/****************/
{
  ISMAPPED
  printf(" ROUTINE TO GENERATE A SOFTWARE INTERRUPT\n");
  printf(" The IRQ Level should be set to 3 and the IRQ Enable must be set !!\n");
  printf("Hit [RETURN] to generate an Interrupt !!\n");
  PTO2
  
  //Set interrupt vector to 0xab. Added by MJ
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intid, 0xab);
  ISOK
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->swirq, 0);
  ISOK

  cont = 1;
  printf("   Press <ctrl>+\\ to stop the routine.\n");
  while(cont) {};
  
  //MJ: Is the processor not supposed to see the interrupt?
}


/****************/
void test_17(void)
/****************/
{
  ISMAPPED

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->cntrst, 0);
  ISOK
  
  printf("   The Counters have been reset\n");
  PTO
}


/****************/
void test_18(void)
/****************/
{
  ISMAPPED
 
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifrst, 0);
  ISOK

  printf("   The FIFO's have been reset\n");
  PTO
}


/****************/
void test_19(void)
/****************/
{
  ISMAPPED
 
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwen, 0);
  ISOK

  printf("The Counter contents have been tranfered to FIFO's\n");
  PTO
}


/****************/
void test_20(void)
/****************/
{
  u_int errflag = 0;
  u_short rpatt1, rpatt2, wpatt;

  ISMAPPED
  printf(" ROUTINE TO TEST FIFO WRITE CONTROL REGISTER\n");

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwcr, 0);
  ISOK  
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifwcr, &rpatt1);
  ISOK 

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwcr, 3);
  ISOK  
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifwcr, &rpatt2);
  ISOK 
  
  if (rpatt1 & 3)
    errflag = 1;
  
  if ((rpatt2 & 3) != 3)
    errflag = 1;

  if (errflag)
    printf("!!! HARDWARE ERROR !!!\n");
  else
    printf(" FIFO WRITE CONTROL REGISTER IS OK\n");

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwcr, 0);
  ISOK 

  printf("          BIT-1                       BIT-0\n");
  printf("      FIFO Write by:              Counters are:\n");
  printf("      --------------          -----------------\n");
  printf("  0        VME                       Disabled\n");
  printf("  1     SEQUENCER                     Enabled\n\n");
  while(1)
  {
    printf("SET A VALUE IN FIFO WRITE CONTROL REGISTER [0-3]:"); 
    wpatt = getdecd(0);
    if (wpatt > 3)
      printf("!! BAD INPUT !!\n");
    else
      break;
  }

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwcr, wpatt);
  ISOK  
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifwcr, &rpatt1);
  ISOK 

  printf("  THE VALUE IN FIFO WRITE CONTROL REGISTER IS   : 0x%04x\n", rpatt1);
}


/****************/
void test_21(void)
/****************/
{
  printf(" ROUTINE TO TEST FIFO READ CONTROL REGISTER\n");
  testreg((uintptr_t)&regs->fifrcr);
}


/****************/
void test_22(void)
/****************/
{
  printf(" ROUTINE TO TEST TRANSFER INTERVAL REGISTER\n");
  testreg((uintptr_t)&regs->seqreg);
}


/****************/
void test_23(void)
/****************/
{
  u_short emptyfl, fullfl;
  u_char bits[16];
  
  ISMAPPED
  printf("      READ FIFO EMPTY AND FULL FLAGS\n");
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifef, &emptyfl);
  ISOK 
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifff, &fullfl);
  ISOK 

  printf("        1111110000000000\n");
  printf("        5432109876543210\n");
  printf("        ----------------\n");

  binstring(bits, emptyfl, 16);
  printf("EMPTY   %s\n", bits);

  binstring(bits, fullfl, 16);
  printf("FULL    %s\n\n", bits);

  printf("    EMPTY = 1 / NOT EMPTY = 0\n");
  printf("    FULL  = 1 / NOT FULL  = 0\n");
}


/****************/
void test_24(void)
/****************/
{
  u_int i, errflag = 0;
  u_short emptyfl, fullfl;
  
  ISMAPPED

  printf("      CHECK FIFO EMPTY AND FULL FLAGS\n");
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwcr, 0);
  ISOK   
  
  printf("The FIFO's will now be filled\n");

  for(i = 1; i < 516; i++)
  {
    ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwen, 0);
    ISOK 
  }
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifef, &emptyfl);
  ISOK 
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifff, &fullfl);
  ISOK   

  if (emptyfl)
  {
    errflag = 1;
    errfullempty (emptyfl, fullfl);
  }
    
  if (fullfl != 0xffff)
  {
    errflag = 1;
    errfullempty (emptyfl, fullfl);
  }

  printf("The FIFO's will now be reset\n");

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifrst, 0);
  ISOK 
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifef, &emptyfl);
  ISOK 
  
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifff, &fullfl);
  ISOK  

  if (emptyfl != 0xffff)
  {
    errflag = 1;
    errfullempty (emptyfl, fullfl);
  }
    
  if (fullfl)
  {
    errflag = 1;
    errfullempty (emptyfl, fullfl);
  }  
 
  if (errflag == 0)
    printf("No ERRORS in CHKFLAGS Test !!]n");
}


/****************/
void test_25(void)
/****************/
{  
  ISMAPPED

  printf("      CHECK FIFO PORTS\n");
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwcr, 0);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifrcr, 0);
  ISOK 
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifrst, 0);
  ISOK 

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->cntrst, 0);
  ISOK 

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwen, 0);
  ISOK 

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwen, 0);
  ISOK 

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwen, 0);
  ISOK 

  printf("The two first positions of all the FIFO's will be diplayed and should contain all '0'\n");

  rdfifos();

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->seqreg, 0xffff);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busymask, 0xffff);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busystate, 0xffff);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifrst, 0);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->cntrst, 0);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwcr, 3);
  ISOK

  ts_delay(1000); //MJ: Per's program did not specify a time

  printf("The two first positions of all the FIFO's will be diplayed and should contain all '1'\n");

  rdfifos();

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->fifwcr, 0);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busymask, 0);
  ISOK
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busystate, 0);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->cntrst, 0);
  ISOK
}


/****************/
void test_26(void)
/****************/
{  
  u_int loop = 0;
  
  ISMAPPED

  printf("!!! This routine will reset the module !!!\n");
  printf("Do you want to run RESET in a LOOP [1/0]: ");
  loop = getdecd(loop);
 
  if (loop)
  {
    printf("   Press <ctrl>+\\ to stop the routine.\n");
    cont = 1;
  }
  else
    cont = 0;
    
  do
  {
    ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->swrst, 1);
    ISOK
  } 
  while (cont);  
  
  printf("The MODULE SOFTWARE RESET Function has been generated\n");
  PTO
}


/****************/
void test_27(void)
/****************/
{
  u_int value = 0;
  u_short rpatt;
  
  ISMAPPED

  printf(" ROUTINE TO GENERATE A TIME-OUT INTERRUPT\n");
  PTO
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 0);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, 3);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intid, 0xab);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->limreg, 0x100);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->ivalreg, 0x200);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busystate, 0);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busymask, 1);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, 0xb);
  ISOK

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 4);
  ISOK

  printf(" The BUSY and TIME-OUT LED's should be off now\n");
  PTO

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busystate, 1);
  ISOK
 
  while(value < 100)
  {
    ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intcsr, &rpatt);
    ISOK

    if (rpatt & 8)
    {
      printf(".");
      value++;
    }
    else
      break;
  }
  
  if (rpatt & 8)
    printf("  No IRQ generated CHECK HARDWARE !!!\n");

  printf(" SREQ and IRQ Operation works OK !!!\n");
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->sreqcsr, 0);
  ISOK  

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, 3);
  ISOK  
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busystate, 0);
  ISOK  
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->busymask, 0);
  ISOK  
  
  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->swrst, 1);
  ISOK  
}


/****************/
void rdfifos(void)
/****************/
{
  u_int fifo;
  u_short rpatt1, rpatt2; 
  u_char bits[16];
  
  ISMAPPED

  printf("                     POSITION 0              POSITION 1\n");
  printf("                 1111110000000000        1111110000000000\n");
  printf("                 5432109876543210        5432109876543210\n");
  printf("                 ----------------        ----------------\n");

  for (fifo = 0; fifo < 16; fifo++)
  {    
    ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifo[fifo], &rpatt1);
    ISOK 
    
    ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->fifo[fifo], &rpatt2);
    ISOK
    
    printf("FIFO %d\n", fifo);
    binstring(bits, rpatt1, 16);
    printf("%s\n", bits);
    binstring(bits, rpatt2, 16);
    printf("%s\n", bits);        
  }

  PTO
}


/*********************************************/
void errfullempty (u_int emptyfl, u_int fullfl)
/*********************************************/
{
  u_char bits[16];
  
  printf("  !!! Hardware ERROR !!!\n");
  printf("        1111110000000000\n");
  printf("        5432109876543210\n");
  printf("        ----------------\n");
  binstring(bits, emptyfl, 16);
  printf("EMPTY   %s\n", bits);
  binstring(bits, fullfl, 16);
  printf("FULL    %s\n", bits);
  printf("\n    EMPTY = 1 / NOT EMPTY = 0\n");
  printf("    FULL  = 1 / NOT FULL  = 0\n");
  PTO
}


/***************************************************/
void binstring(u_char *bits, u_int data, u_int nbits)
/***************************************************/
{
  u_int loop;
  
  for (loop = 0; loop < nbits; loop++)
  {
    if (data & (1 << loop))
      bits[loop] = '1';
    else
      bits[loop] = '0';
  }
}


/******************/
void read_prom(void)
/******************/
{
  u_int data;

  printf("READ IDPROM\n");
  
  readprom4(MANUFID, &data);
  printf("The manufacturer ID is 0x%08x\n", data);
  if (data != CERNID) 
    printf("For CERN this should be 0x%08x\n", CERNID); 

  readprom4(BOARDID, &data);
  printf("The serial number is 0x%08x\n", data);
  
  readprom4(REVISION, &data);
  printf("The revision number is 0x%08x\n", data);
}


/*****************************************/
void readprom4(u_int offset, u_int *concat)
/*****************************************/
{
  u_int loop;
  u_short data[4];
  
  ISMAPPED
  for(loop = 0; loop < 4; loop++)
  {
    ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->cr[offset + loop], &data[loop]);
    ISOK
    data[loop] &= 0xff;
  }

  *concat = ((u_int)data[0] << 24) | ((u_int)data[1] << 16) | ((u_int)data[2] << 8) | (u_int)data[3];
}


/*******************/
void write_prom(void)
/*******************/
{
  u_int ma, bo, re;
  u_short data;
  char mode[9];
  
  mode[0] = 'c';
  printf("           WRITE IDPROM\n");

  ISMAPPED
  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->cr[1], &data);
  ISOK

  while(mode[0] == 'c' || mode[0] == 'C')
  {
    ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->cr[1], data);
    if (ret)
    {
      VME_ErrorPrint(ret);
      printf("FIT STRAP ST500 + ENTER [c] TO CONTINUE OR [q] TO QUIT :\n");
      fgets(mode, 9, stdin);
    }
    else
      break;
  }

  if (ret)
    return;
        
  printf("The following values are programmed. [CR] to keep value :\n");
  readprom4(MANUFID, &ma);
  readprom4(BOARDID, &bo);
  readprom4(REVISION, &re);
  
  printf("Manufacturer ID: ");
  ma = gethexd(ma);
  printf("Serial number: ");
  bo = gethexd(bo);
  printf("Revision: ");
  re = gethexd(re);
  
  writeprom4(MANUFID, ma);
  writeprom4(BOARDID, bo);
  writeprom4(REVISION, re);
}


/*****************************************/
void writeprom4(u_int offset, u_int concat)
/*****************************************/
{
  u_int loop, data[4];
  
  ISMAPPED
  data[0] = (concat >> 24) & 0xff;
  data[1] = (concat >> 16) & 0xff;
  data[2] = (concat >> 8) & 0xff;
  data[3] = concat & 0xff;

  for(loop = 0; loop < 4; loop++)
  {
    ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->cr[offset + loop], data[loop]);
    ISOK
    ts_delay(5000);
  }
}


/************************/
void testreg(u_int offset)
/************************/ 
{  
  char mode[9];
  u_int inv, bitpos, errflag, dmode;
  u_short opatt, rpatt, wpatt;
  
  ISMAPPED
  printf("To test register press:        [T/t]\n");
  printf("To read only the value press:  [R/r]\n");
  printf("To read and enter value press: [E/e]\n");
  printf("To skip this routine press:    [Q/q]\n");  
  fgets(mode, 9, stdin);
  
  if (mode[0] == 'q' || mode[0] == 'Q')
    return;
    
  if (mode[0] == 't' || mode[0] == 'T')
   dmode = 1;
  if (mode[0] == 'r' || mode[0] == 'R')
   dmode = 3;
  if (mode[0] == 'e' || mode[0] == 'E')
   dmode = 2;

  if (dmode == 1)
  {
    wpatt = 1;
    errflag = 0;
    for (bitpos = 0; bitpos < 16; bitpos++)
    {
      for (inv = 0; inv < 2; inv++)
      {
        if (inv)
	  opatt = ~wpatt;
	else
	  opatt = wpatt;

        ret = VME_WriteSafeUShort(handle, offset, opatt);
        ISOK
        ret = VME_ReadSafeUShort(handle, offset, &rpatt);
        ISOK
	if (rpatt != opatt)
	{
	  errflag = 1;
	  printf("Error at bit: %d\n", bitpos);
	  printf("Written: 0x%04x  Read: 0x%04x\n", opatt, rpatt);
	}
      }
      wpatt = wpatt << 1;
    } 

    if (!errflag)
      printf("No R/W erros detected\n");
  }
  
  if (dmode == 2)
  {
    ret = VME_ReadSafeUShort(handle, offset, &rpatt);
    ISOK  
    wpatt = rpatt;

    printf("The register is at address offset 0x%04x\n", offset);
    printf("Enter new value ['x????] or press [CR]: ");
    wpatt = gethexd(wpatt);
    ret = VME_WriteSafeUShort(handle, offset, wpatt);
    ISOK
  }

  if (dmode > 1)
  {
    ret = VME_ReadSafeUShort(handle, offset, &rpatt);
    ISOK
    
    printf("The new value of the register is 0x%04x\n", rpatt);
  }
}


/*********************************/
void SigQuitHandler(int /*signum*/)
/*********************************/
{
  cont = 0;
}


/*********************************/
void vme_signal_handler(int signum)
/*********************************/
{
  u_short rpatt, wpatt;
  
  if (signum == SIGNUM )  
  {

  printf("       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  printf("         VME INTERRUPT RECEIVED\n");
  printf("       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");

  ret = VME_ReadSafeUShort(handle, (uintptr_t)&regs->intcsr, &rpatt);
  ISOK
  
  wpatt = rpatt & 0x7;

  ret = VME_WriteSafeUShort(handle, (uintptr_t)&regs->intcsr, wpatt);
  ISOK

  printf("         The INTERRUPT is now disabled\n");
  printf("       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"); 

  }
  else 
    printf("ERROR: unexpected signal\n") ;
}

