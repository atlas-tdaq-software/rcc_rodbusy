/********************************************************/
/*  rodbusy.h						*/
/*							*/
/*  Author: Markus Joos, CERN EP/ESS			*/
/*** C 2005 - The software with that certain something***/

#ifndef _RCC_RODBUSY_H
#define _RCC_RODBUSY_H

//Offsets in the Configuration EEPROM
#define MANUFID   16
#define BOARDID   20
#define REVISION  24

typedef struct
{
  u_short cr[64];       // Configuration EEPROM
  u_short swrst;        // 0x80
  u_short swirq;        // 0x82
  u_short intcsr;       // 0x84
  u_short intid;        // 0x86
  u_short dummy1[4];
  u_short sreqcsr;      // 0x90
  u_short sreqsetclr;   // 0x92
  u_short limreg;       // 0x94
  u_short ivalreg;      // 0x96
  u_short busystate;    // 0x98
  u_short busymask;     // 0x9a
  u_short dummy2[18];
  u_short fifo[16];     // 0xc0 - 0xde
  u_short cntrst;       // 0xe0
  u_short fifrst;       // 0xe2
  u_short fifwen;       // 0xe4
  u_short fifwcr;       // 0xe6
  u_short fifrcr;       // 0xe8
  u_short seqreg;       // 0xea
  u_short fifff;        // 0xec
  u_short fifef;        // 0xee
} T_rodbusy;
  
typedef struct
{  
  u_short nvalid;
  u_short data[512];
} T_fifo_data;
  
typedef struct
{  
  T_fifo_data fifo[16];
} T_rodbusy_fifos; 
  
typedef struct
{  
  u_int busy[16];
} T_rodbusy_busy;

typedef struct
{ 
  u_int irq_vector;
  u_int irq_level;
  u_int irq_enable;
  u_int sreq_enable;
  u_int limit;
  u_int interval;
  u_int busy_mask;  
  u_int fifo_control;  
  u_int mode;  
  u_int transfer_interval;  
} T_rodbusy_init;
 
enum rodbusy_errors 
{
  RODBUSY_OK,
  RODBUSY_ISOPEN = (P_ID_RODBUSY << 8) + 1,
  RODBUSY_ISNOTOPEN,
  RODBUSY_ERROR_FAIL,
  RODBUSY_VMEOPEN,
  RODBUSY_VMEMSTMAP,
  RODBUSY_VMEMSTUNMAP,
  RODBUSY_VMECLOSE,
  RODBUSY_ILLVAL,
  RODBUSY_ILLMODE,
  RODBUSY_NO_CODE
};
     
#define RODBUSY_OK_STR                      "OK"
#define RODBUSY_ISOPEN_STR                  "The RODBUSY library is already open"
#define RODBUSY_ISNOTOPEN_STR               "The RODBUSY library is not opened"     
#define RODBUSY_VMEMSTMAP_STR               "Error on VME_MasterMap"
#define RODBUSY_VMEMSTUNMAP_STR             "Error on VME_MasterUnmap"
#define RODBUSY_VMECLOSE_STR                "Error on VME_Close"
#define RODBUSY_VMEOPEN_STR                 "Error on VME_Open"
#define RODBUSY_NO_CODE_STR                 "Unknown error"
#define RODBUSY_ILLVAL_STR                  "Illegal value"
#define RODBUSY_ILLMODE_STR                 "The requested function is incompatible with the current mode"
#ifdef __cplusplus
extern "C" {
#endif

/* prototypes */
err_type RODBUSY_Open(u_int vme_address);
err_type RODBUSY_Close(void);
err_type RODBUSY_Dump(void);
err_type RODBUSY_ReadFIFO(T_rodbusy_fifos *data);
err_type RODBUSY_Init(T_rodbusy_init *data);
err_type RODBUSY_Reset(void);
err_type RODBUSY_GenerateSWInterrupt(void);
err_type RODBUSY_ClearInterrupt(void);
err_type RODBUSY_GetBusy(T_rodbusy_busy *data);
err_type RODBUSY_GetBusyMask(T_rodbusy_busy *data);
err_type RODBUSY_GetGlobalBusy(u_int *globalbusy);
err_type RODBUSY_CounterReset(void);
err_type RODBUSY_FIFOReset(void);
err_type RODBUSY_FIFOWrite(void);

#ifdef __cplusplus
}
#endif

#endif
